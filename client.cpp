#include "client.h"

// game protocol
#include "gameprotocol.h"
// qt
#include <QDataStream>  //provides serialization of binary data to a QIODevice
#include <QHostAddress>
#include <QDebug>
//#include <string.h>

Client::Client(QObject *parent) :
    QObject(parent)
{    time=0;

     connection=false;
     request_id=0;
     read_header=false;
     size_of_body_pkg=0;
     client_id=0;

        //game
     game_port=0;
     game_connection=false;
     game_read_header=false;



     connect(&socket, SIGNAL(connected()), this, SLOT(connectedToServer()));//dungcho QML
     connect(&socket, SIGNAL(disconnected()), this, SLOT(disconnectedToServer()));
     connect(&socket,SIGNAL(bytesWritten(qint64)),this,SLOT(sentSth()));
         // xem co nhan duoc tra loi cua server hay ko
     connect(&socket,SIGNAL(readyRead()),this,SLOT(receiveDataFromServer()));

         //game sever
     connect(&game_socket, SIGNAL(connected()), this, SLOT(connectedToGameServer()));
     connect(&game_socket,SIGNAL(disconnected()),this,SLOT(disconnectedGameServer()));
     connect(&game_socket,SIGNAL(bytesWritten(qint64)),this,SLOT(sentSth()));
         // xem co nhan duoc tra loi cua server hay ko
     connect(&game_socket,SIGNAL(readyRead()),this,SLOT(receiveGameServer()));


}

Client::~Client() {

}

void Client::connectServer(QString address,QString port){
    qDebug() <<" client sent connect to server";
    qDebug() <<" ip:" << address;
    qDebug() <<" port" << port;
    socket.abort();
    socket.connectToHost(address,port.toInt() );
    if (socket.waitForConnected(1000))
    {   // qDebug("Connected to server!");
        connection=true;
    }

    else
    {    connection=false;
        QString error;
        error="Sorry,could not connect to this server !!!";

        emit signError(error); //CHUYEN DEN QML
        qDebug(" Does not connect to server!");
    }
    emit(signConnectStatus(connection));
    //  emit signIp(address);
    //  emit signPort(port.toInt());
}

void Client::JoinRequest(QString address,int port,QString name){
    game_ip= address;//ko can
    game_port=port;//ko can
    qDebug() <<" player_name:"<<name;

    gp_join_request query;
    query.client_id=client_id;
    query.flags.client_type = GP_JOIN_CLIENT_TYPE_PLAYER;
    query.flags.join_flag   = GP_JOIN_FLAG_JOIN;
    strcpy(query.player_name, name.toLatin1()); //convert qstring to char[]
    qDebug() <<" player_name:"<<query.player_name;
    player_name=name;
    sendJoinRequest(query);



}

void  Client::connectGame(QString game_ip,int game_port){

    qDebug() <<" client sent connect to game server";
    game_socket.abort();
    game_socket.connectToHost(game_ip,game_port );
    if (socket.waitForConnected(1000))
    {   // qDebug("Connected to game server!");
        game_connection=true;
    }

    else
    {    game_connection=false;
        qDebug(" Does not connect to game server!");
    }

}

void Client::disconnectGameServer(){
    game_socket.abort();
    game_socket.disconnectFromHost();

    if (game_socket.state() == QAbstractSocket::UnconnectedState ||
            game_socket.waitForDisconnected(1000))

    { //qDebug("Disconnected from game server!");
        game_connection=false;  }
}

void    Client::disconnectGame(){

    game_socket.disconnectFromHost();

    if (game_socket.state() == QAbstractSocket::UnconnectedState ||
            game_socket.waitForDisconnected(1000))

    { // qDebug("Disconnected from game server!");
        game_connection=false;  }


    socket.disconnectFromHost();
    if (socket.state() == QAbstractSocket::UnconnectedState ||
            socket.waitForDisconnected(1000))

    {// qDebug("Disconnected from server!");
        connection=false;  }


}

void Client::connectedToServer(){
    connection=true;
    qDebug() <<"Connected successfully";
    gp_connect_request a;
    a.connect_flag=GP_CONNECT_FLAG_CONNECT;
    sendConnectRequestToServer(a);

}
void Client::connectedToGameServer(){
    game_connection = true;
    qDebug() <<"connected to game server";

}

void Client::disconnectedGameServer(){
    game_connection=false;
    qDebug() <<"disconnected from game server";

}
void Client::disconnectedToServer(){
    connection=false;
    qDebug() <<"Disconnected from server";

}

void Client::disconnectServer(){
    socket.abort();
    socket.disconnectFromHost();
    game_port=0;
    if (socket.state() == QAbstractSocket::UnconnectedState ||
            socket.waitForDisconnected(1000))

    {qDebug("Disconnected from server 12345!");
        connection=false;  }
}


void Client::sendConnectRequestToServer( gp_connect_request&  query ){

    time=1;
    // Build header
    gp_header header;
    header.size = sizeof(gp_header)-sizeof(gp_header_prefix);  // ??? definition
    header.type = GP_REQUEST_TYPE_CONNECT;
    header.flags.answer = 0;
    header.request_id = ++request_id;

    // Build block

    QByteArray block1;
    QDataStream out(&block1, QIODevice::WriteOnly );
    out.setVersion( QDataStream::Qt_5_1 );

    // Write header
    out.writeRawData( (char*)(&header), sizeof(gp_header) );

    // Write body
    out.writeRawData( (char*)(&query), sizeof(gp_connect_request) );

    // "Send" block ,Writes the content of byteArray to the device.
    socket.write( block1 );

}


void     Client::sentSth() {
    switch(time)
    {
    case 1:
        qDebug() << "you sent connect request to server";
        break;
    case 2:
        qDebug() << "you sent server query to server";
        break;

    case 3:
        qDebug() << "you sent joint request to server";
        break;
    case 4:
        qDebug() << "you sent client verification answer to server";
        break;
    case 5:
        qDebug() << "you sent game request to game server";
        break;
    default:
        qDebug() << "you sent sth to server";
        break;

    }

}

void Client::receiveDataFromServer()
{
    // quint64==unsigned long long int
    unsigned long long int t1;
    t1 = socket.bytesAvailable();
    qDebug() << "Data is coming with : "<< t1 <<" bytes";
    QDataStream in(&socket);
    in.setVersion( QDataStream::Qt_5_1 );

    if( read_header == false )
    {

        size_of_body_pkg = 0;
        //ktra so byte dang den so voi so byte of header
        if( t1 < (int)sizeof(gp_header) )
            return;

        // Read header
        in.readRawData( (char*)(&client_header), sizeof(gp_header) );

        if( client_header.prefix.id != client_header_prefix.id )
            return;


        if (client_header.flags.answer==true)
        {
            qDebug() << "type of header: "<< client_header.type ;

            switch( client_header.type )
            {
            case GP_REQUEST_TYPE_CONNECT:
                size_of_body_pkg = sizeof(gp_connect_answer);
                read_header = true;
                qDebug() << "size of body package (connect): "<< size_of_body_pkg ;
                break;

            case GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY:
                size_of_body_pkg=sizeof(gp_default_server_query_answer);
                read_header=true;
                qDebug() << "size of body package (server query): "<< size_of_body_pkg ;
                break;
            case GP_REQUEST_TYPE_JOIN:
                size_of_body_pkg = sizeof(gp_join_answer);
                read_header = true;
                qDebug() << "size of body package (join): "<< size_of_body_pkg ;
                break;

            case GP_REQUEST_TYPE_ERROR:
                size_of_body_pkg = sizeof(gp_client_error_response);
                read_header=true;
                qDebug() << "size of body package(error): "<< size_of_body_pkg ;
                break;




            default:
                break;
            }
        }
        else {

            // Get size from valid direct request package
            switch( client_header.type ) {
            case GP_REQUEST_TYPE_ERROR:

                size_of_body_pkg = sizeof(gp_client_error_response);
                read_header=true;
                break;

            default:
                return;
            }
        }  //end if (client_header.flags.answer==true)

    }  //end if (read_header==false)

    // read body of package
    unsigned long long int t2;
    t2 = socket.bytesAvailable();
    qDebug() << "Data 2 is coming  with : "<< t2 <<" bytes ";

    // Return and wait for more data if data available is less then the required body data size
    if( t2 < size_of_body_pkg )

        return;

    // Handle request ,read body of the package
    switch( client_header.type ) {

    case GP_REQUEST_TYPE_ERROR:
    {
        gp_client_error_response answer;
        in.readRawData( (char*)(&answer), sizeof(gp_client_error_response) );

        connection = false;

        qDebug() << "error :"<< answer.error;

        disconnectServer();




    } break;

    case GP_REQUEST_TYPE_CONNECT:
    {
        gp_connect_answer answer;
        in.readRawData( (char*)(&answer), sizeof(gp_connect_answer) );

        emit signHandleConnectAnswerPkg( client_header, answer );//de bao cho guiapplication biet
        // handle connect request
        quint8 status = answer.state;
        if( status == GP_CONNECT_FLAG_CONNECT )
        { client_id = answer.client_id;
            qDebug()<< "your client_id is: " << client_id;}
        else
            client_id = 0;

        gp_default_server_query query;

        // server info
        query.request_flags.server_info = true;        //bang true co nghia la stick chon no
        query.server_fields.connect_port = true;
        query.request_flags.player_list = true;

        // map info
        query.request_flags.map_info = true;
        query.map_fields.width    = true;
        query.map_fields.height   = true;

        sendServerQuery(query );


    } break;


    case GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY:
    {
        gp_default_server_query_answer answer;
        in.readRawData( (char*)(&answer), sizeof(gp_default_server_query_answer) );

        if(answer.server_info.fields.connect_port)  //means it exists???
        {  game_port = answer.server_info.connect_port;
            map_height=answer.map_info.height;
            map_width=answer.map_info.width;
            qDebug()<< "name:" << answer.player_list.info_fields.id;



            qDebug()<< "a number of players:" << answer.player_list.count;
            qDebug()<< "game port is :" << game_port;
            emit signHandleDSQAnswerPkg( client_header,answer );


            game_ip=socket.peerAddress().toString();
            qDebug() << "game ip is :" << game_ip;
            emit signGameIp(game_ip);

        }
        else
            qDebug()<< "you can not get game port";
    } break;


    case GP_REQUEST_TYPE_JOIN:

    {
        gp_join_answer answer;
        in.readRawData((char*)(&answer),sizeof(gp_join_answer));

        if( answer.state == GP_JOIN_FLAG_JOIN )
        {
            valid_code = answer.validation_code;
            qDebug() << "your validation code is :" << valid_code;

            connectGame(game_ip,game_port);
        }

        else

        {
            valid_code = 0;
            qDebug() << "you can not get validation code .";
        }
    } break;


    default:
        qDebug()<< "you can not get anything";
        break;
    }

    //reset read_header value to read new pakage
    read_header = false;

    // find new package coming
    if( socket.bytesAvailable() >= sizeof(gp_header) )
        receiveDataFromServer();

}

void Client::sendServerQuery( gp_default_server_query &query)
{
    time=2;
    // buid header
    gp_header header;
    header.size=sizeof(gp_header)-sizeof(gp_header_prefix);
    header.type=GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY;
    header.flags.answer=0;
    header.request_id=++request_id;

    //build block to contain data
    QByteArray block2;
    QDataStream out(&block2,QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_1);

    //write header of package
    out.writeRawData((char*)&header,sizeof(gp_header));
    // write body of package
    out.writeRawData((char*)&query,sizeof(gp_default_server_query));
    //send block to server
    socket.write(block2);

}

void Client::sendJoinRequest(gp_join_request &query){
    time=3;
    // buid header
    gp_header header;
    header.size=sizeof(gp_header)-sizeof(gp_header_prefix);
    header.type=GP_REQUEST_TYPE_JOIN;
    header.flags.answer=0;
    header.request_id=++request_id;

    //build block to contain data
    QByteArray block3;
    QDataStream out(&block3,QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_1);

    //write header of package
    out.writeRawData((char*)&header,sizeof(gp_header));
    // write body of package
    out.writeRawData((char*)&query,sizeof(gp_join_request));
    //send block to server
    socket.write( block3 );


}

void Client::receiveGameServer(){

    quint16 byte;
    byte = game_socket.bytesAvailable();
    qDebug() << "data is coming with: "<< byte << "bytes ";

    QDataStream in(&game_socket);
    in.setVersion(QDataStream::Qt_5_1);
    if(game_read_header==false)
    {
        game_size_body=0; //reset body size of coming package
        if(byte<(int)sizeof(gp_header))
            return;

        //read header of package
        in.readRawData((char*)(&game_client_header),sizeof(gp_header));
        //if header prefix is wrong --> return
        if (game_client_header.prefix.id != game_header_prefix.id)
            return;

        //check" answer" of package
        if(game_client_header.flags.answer==0)  //do nhan client verificaiton request of server truoc
        {
            qDebug() << "type of header: "<< game_client_header.type ;
            switch(game_client_header.type)
            {
            case GP_REQUEST_TYPE_CLIENT_VERIFICATION:
                game_size_body=0;
                game_read_header=true;
                break;
            case GP_REQUEST_TYPE_ERROR:
                game_size_body=sizeof(gp_client_error_response);
                game_read_header=true;
                break;
            default:
                return;

            } //end Switch

        } //end if(game_client_header.flags.answer==0)

        else
        {  qDebug() << "type of header: "<< game_client_header.type ;

            switch(game_client_header.type)
            {
            case GP_REQUEST_TYPE_GAME:
                game_size_body=sizeof(gp_game_update);
                game_read_header=true;
                break;
            case GP_REQUEST_TYPE_ERROR:
                game_size_body=sizeof(gp_client_error_response);
                game_read_header=true;
                break;
            default:
            {  qDebug() <<" not in 2 above  cases";
                return;}

            }
        }


    } //end if(game_read_header==false)

    quint16 t2;
    t2=game_socket.bytesAvailable();
    qDebug() <<"data 2 is coming with: "<< t2 <<"bytes";

    //return and wait for more data if data available is less than the required body data size
    if(t2 < game_size_body)
        return;

    switch (game_client_header.type)
    {
    case GP_REQUEST_TYPE_CLIENT_VERIFICATION:
    { time =4;
        //send client verificaiton answer to server
        //build header
        gp_header header;
        header.size=sizeof(gp_header)-sizeof(gp_header_prefix);
        header.type=GP_REQUEST_TYPE_CLIENT_VERIFICATION;
        header.flags.answer=1;
        header.request_id=game_client_header.request_id;

        //build body

        gp_client_verification_answer body;
        body.client_id=client_id;
        body.vcode=valid_code;
        //build block
        QByteArray    block;
        QDataStream out(&block,QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_5_1);

        //write header
        out.writeRawData((char*)(&header),sizeof(gp_header));
        //write body
        out.writeRawData((char*)(&body),sizeof(gp_client_verification_answer));
        //send block
        game_socket.write(block);
    }break;
    case GP_REQUEST_TYPE_ERROR:
    {
        gp_client_error_response answer;
        in.readRawData((char*)(&answer),sizeof(gp_client_error_response));
        game_connection = false;

        qDebug() << "error :"<< answer.error;

        disconnectGameServer();

    }break;

    case GP_REQUEST_TYPE_GAME:
    {
        gp_game_update update;
        in.readRawData((char*)(&update),sizeof(gp_game_update));
        emit signUpdateGame(update,player_name);

    }break;

    }// end switch

    game_read_header=false;
    if(game_socket.bytesAvailable()>= sizeof(gp_header))
        receiveGameServer();//ask
}


void Client::sendGameRequest(gp_game_request &request){
    time =5;
    //build header
    gp_header header;

    header.size=sizeof(gp_header)-sizeof(gp_header_prefix);
    header.type=GP_REQUEST_TYPE_GAME;
    header.flags.answer=0;
    header.request_id=++request_id;
    //build block
    QByteArray block;
    QDataStream out(&block,QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_1);

    //write header

    out.writeRawData((char*)(&header),sizeof(gp_header));
    //write body
    out.writeRawData((char*)(&request),sizeof(gp_game_request));
    //send block
    game_socket.write(block);
}


void Client::buildGameRequest(QString &cmd, QStringList &params){

    gp_game_request request;

    // Check that the command is between 1 and 16 chars.
    if( cmd.size() < 1 || cmd.size() > 16 )
        return;

    // Copy the command to the request struct
    strcpy( request.cmd, cmd.toStdString().c_str() );

    // Set the number of command parameters
    request.param_count = params.size();

    // Copy over the parameters to the request struct
    QStringList::ConstIterator itr;
    int i;
    for( i = 0, itr = params.begin(); itr != params.end(); itr++ ) {

        if( (*itr).size() < 1 || (*itr).size() > 16 )
            return;

        strcpy( request.params[i++].param, (*itr).toStdString().c_str() );
    }

    qDebug() << "Entered command:";
    qDebug() << "  cmd: " << request.cmd;
    for( unsigned int i = 0; i < request.param_count; i++ )
        qDebug() << "  param[" << i << "]: " << request.params[i].param;

    sendGameRequest(request);
}

void Client::movePlayer(QString id,QPointF pos){
    qDebug() << "you move";
    QStringList sl;
    qDebug() << "new x=x +"<<pos.x();
    qDebug() << "new y=y +"<<pos.y();
    QString str="move";
    sl<< QString::number(pos.x());   //map
    sl<< QString::number(pos.y());
    buildGameRequest(str,sl);

}
void Client::shootPlayer(QString id,QPointF pos){
    qDebug() << "you sent shooting request";
    QStringList sl;

    QString str="shoot";
    sl<< QString::number(pos.x());
    sl<< QString::number(pos.y());
    buildGameRequest(str,sl);

}



