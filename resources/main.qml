import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2
import QtQuick.Window 2.0
import QtMultimedia 5.0


Item { id: root

    Audio {
        id: sound1
        source: "qrc:/qml/music"

    }

    property int player
    property int position_x
    property int position_y
    property int map_height
    property int map_width



    property int client_id
    property int game_port
    property string server_name
    property string game_ip
    property string error
    property bool status

    signal connectServer(string s1, string s2)
    signal connectGameServer(string a1, int a2,string name1)
    signal disconnectGameServer()
    signal disconnectServer()

    signal movePlayer( string player, point dir )
    signal shootPlayer(string player,point dir)
    // mock-up code example:
    //   signal to tell the second rectangle whether the connect
    //   to connection server was successful
    signal connectToServerStatus(bool status)


    onConnectToServerStatus: btconnect2.visible = status
    onDisconnectServer:{ error=""
        game_ip=""
        game_port=""
        playername.visible=false
    }

    states:[ State { name:"state_lobby_window";
            PropertyChanges {
                target: rect_lobby_window;
                visible: true

            }
            PropertyChanges {
                target: rect_wellcome_window;
                visible: false

            }

        }  //state1
        ,

        State{ name:"state_play_window";

            PropertyChanges {
                target: rect_play_window;
                visible:true

            }

            PropertyChanges {
                target: rect_wellcome_window;
                visible:false

            }
        } //state2

    ] //state


    //state 1
    Rectangle {
        id:rect_wellcome_window
        visible: true
        color: "gray";
        anchors {top:parent.top; bottom:parent.bottom;left:parent.left; right:parent.right }

        Rectangle{
            id:rect1
            width: 110; height: 35
            Text{text: " IP Address: ";font.pixelSize: 20}
            x: 300; y:150
            color:parent.color   }

        Rectangle{
            id:rect2

            width: 110; height: 35
            Text{text: " Port: ";font.pixelSize: 20}
            x: 500; y:150
            color:parent.color
        }
        TextField {
            id:serveraddress
            width: 160
            x: 300; y:200
            text: ("127.0.0.1");
            font.pixelSize: 18

        }
        TextField {
            id:serverport

            width: 80
            x: 500;y:200
            text: ("1234")
            font.pixelSize: 18

        }
        Button {
            id: btconnect
            width: 110;
            height: 50
            x: 400;
            y:300

            Text{ text: " connect ";font.pixelSize: 18}

            onClicked: {
                console.debug("Button.onClicked, addr: " + serveraddress.text) //check
                connectServer(serveraddress.text,serverport.text); root.state="state_lobby_window"
            }

        }

    }
    //state 2

    Rectangle {
        id:rect_lobby_window
        visible: false
        anchors {top:parent.top; bottom:parent.bottom;left:parent.left; right:parent.right }

        Rectangle{
            id:rect3

            width: 200; height: 100
            x: 250; y:50
            color:"white"
            GridLayout{
                columns: 2

                Text{ text: " GAME INFORMATION ";font.pixelSize: 22;font.italic: true;color: "red"
                }   Text{ text: server_name }
                Text{ text: " game server IP: ";font.pixelSize: 18}   Text{ text: game_ip;font.pixelSize:18 }
                Text{ text: " game port: ";font.pixelSize: 18}        Text{ text: game_port ;font.pixelSize: 18}

            }
        }

        Rectangle {

            id: recterror
            width: 200; height: 50
            x: 250; y:250
            Text{text: error;font.pixelSize: 18;color:"red"}
        }
        Text {
            text: "Your name:"
            font.pointSize: 18
            color: "green"
            x:250;y:150
        }
        TextField {
            id:playername

            width:180
            x:400;y:150
            text: (" ")
            font.pixelSize: 18

        }



        Button {
            id: btconnect2
            width: 100; height: 35
            x: 300; y:300
            Text{ text: " connect ";font.pixelSize: 18}
            onClicked: {connectGameServer(game_ip,game_port,playername.text);root.state="state_play_window"}

        }

        Button {
            id: btcomeback  //button come back
            width: 100; height: 35
            x: 420; y:300
            Text{ text: " come back ";font.pixelSize: 18}
            onClicked: {disconnectServer();root.state="state_wellcome_window"}

        }

    }
    //state 3
    Rectangle {
        id:rect_play_window
        visible: false
        color: "white"
        anchors {top:parent.top; bottom:parent.bottom;left:parent.left; right:parent.right  }


        ////vi du cua thay///////////////////////////////////////////

        Component {
            id: list_delegate

            RowLayout{
                height: 80; spacing: 50; width:150
                Rectangle {Image{source:is_enemy ?"qrc:/qml/icon1":"qrc:/qml/icon3"} }

                Item{ Layout.fillWidth: true }
                Text{ color:is_enemy ? "black" : "red";text:is_enemy? "enemy:"+name + " (" + score + ")":"you"+ " (" + score + ")" }
            }
        }

        Component {
            id: player_delegate

            RowLayout {
                id: mushroom
                x: position.x*(view.width/map_width)
                y: position.y*(view.height/map_height)
                Rectangle {Image{source:is_enemy ?"qrc:/qml/icon1":"qrc:/qml/icon3"}}
                Text{ font.bold:true;height: 30; text: is_enemy?name:"you";color:is_enemy ?"blue":"red"}
            }
        }
        // component of bullet
        Component {
            id: bullet_delegate

            RowLayout {
                id: bullet
                x: position.x*((view.width)/map_width)
                y: position.y*((view.height)/map_height)

                Rectangle {Image{source:is_enemy ?"qrc:/qml/icon12":"qrc:/qml/icon32"}}

            }
        }


        ColumnLayout {
            anchors.fill: parent

            Rectangle {
                id: view

                Image{ anchors.fill: parent;source: "qrc:/qml/overview";}
                Layout.fillWidth: true
                Layout.fillHeight: true

                clip: true

                Repeater {
                    anchors.fill: parent
                    model: VisualDataModel {
                        model: player_model
                        delegate: player_delegate
                    }

                }

                Repeater {
                    anchors.fill: parent
                    model: VisualDataModel {
                        model: bullet_model
                        delegate: bullet_delegate
                    }

                }


                FocusScope {
                    focus: true
                    Keys.onPressed: {

                        if(event.key == Qt.Key_Left)
                            movePlayer( client_id.toString(), Qt.point(-20,0) )
                        else if(event.key == Qt.Key_Right)
                            movePlayer( client_id.toString(), Qt.point(20,0) )
                        else if(event.key == Qt.Key_Up)
                            movePlayer( client_id.toString(), Qt.point(0,-20) )
                        else if(event.key == Qt.Key_Down)
                            movePlayer( client_id.toString(), Qt.point(0,20) )
                        else if(event.key==Qt.Key_A)
                        {  shootPlayer(client_id.toString(), Qt.point(-20,0))
                            sound1.play()
                        }
                        else if(event.key==Qt.Key_F)
                        { shootPlayer(client_id.toString(), Qt.point(20,0))
                            sound1.play()
                        }
                        else if(event.key==Qt.Key_E)
                        {  shootPlayer(client_id.toString(), Qt.point(0,-20))
                            sound1.play()
                        }
                        else if(event.key==Qt.Key_X)
                        {
                            shootPlayer(client_id.toString(), Qt.point(0,20));
                            sound1.play()
                        }


                    }
                }

            }
            ListView{

                Layout.fillWidth: true
                width: parent.width
                height:parent.height*(1/5)
                orientation:ListView.Horizontal
                model: VisualDataModel {
                    model: player_model
                    delegate: list_delegate
                }
            } //listview


        } //columnlayout
        Button {
            id: btdisconnect  //button disconnect game server
            width: 70; height: 35
            x: 900*((view.width)/map_width); y:1150*((view.height)/map_height)
            visible:true
            Text{ text: " Exit ";font.pixelSize: 18;}
            onClicked: {disconnectGameServer();root.state="state_wellcome_window";}

        }

    }  //state 3

} //item
