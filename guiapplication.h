#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H
#include "gameprotocol.h"
#include "window.h"
// qt
#include <QGuiApplication>

class GuiApplication : public QGuiApplication {

    Q_OBJECT
public:
    GuiApplication( int& argc, char** argv );

    void initialize();
public slots:
    void handleConnectAnswerPkg( gp_header header, gp_connect_answer answer );
    void handleDSQAnswerPkg(gp_header header,gp_default_server_query_answer answer);
    void handleGameIp(QString game_ip);
    void handleError(QString error);
signals:
    void  signconnect(QString address,QString port);
    void  signconnectGame(QString address,int port,QString name);
    void  signdisconnectGame();
    void  signdisconnect();

    void  signConnectStatus(bool status);
    void  signUpdateGame(gp_game_update update,QString player_name);
    void  signmovePlayer(QString id ,QPointF pos);
    void  signshootPlayer(QString id ,QPointF pos);
private:
    Window _window;


};
#endif // GUIAPPLICATION_H
