
#include "bulletdatamodel.h"

void
BulletDataModel::addBullet(const QString& name, const QPoint& pos, bool is_enemy) {
    qDebug() << "rowcount before insert :" <<rowCount();
    int a;
    b_name=name;
    b_pos=pos;
    if(bullet_data.size()>0)
    {
        for(int i=0;i<bullet_data.size();i++)
        {
            if(bullet_data[i].bulletname==name)
            { a=1;  }
            else { a=0;}
        }


    }
    else if(bullet_data.size()==0)

    {
        beginInsertRows(QModelIndex(),rowCount(),rowCount());
        bullet_data.append(Private::BulletData(name,pos,is_enemy));
        endInsertRows();
    }
    if(a==0){
        beginInsertRows(QModelIndex(),rowCount(),rowCount());
        bullet_data.append(Private::BulletData(name,pos,is_enemy));
        endInsertRows();
    }
    qDebug() << "rowcount after insert :" <<rowCount();

}
void BulletDataModel::removeBullet(const QString& name) {

    qDebug() << "rowcount before remove :" <<rowCount();
    for(int i=0;i<bullet_data.size();i++){
        if(bullet_data[i].bulletname==name){
            beginRemoveRows(QModelIndex(),i,i);
            bullet_data.remove(i);
            emit(dataChanged(index(i), index(i)));
            endRemoveRows();
        }


    }
    qDebug() << "rowcount after remove :" <<rowCount();
}
BulletDataModel::PlayerDataTuple
BulletDataModel::findBullet(const QString& name) {

    auto item = std::find_if( std::begin(bullet_data), std::end(bullet_data),
                              [name](const Private::BulletData& data) { return data.bulletname == name; }
    );

    return PlayerDataTuple(item,index(item-std::begin(bullet_data)));
}


QHash<int,QByteArray>
BulletDataModel::roleNames() const {

    QHash<int,QByteArray> roles;
    roles[DataRoles::NameRole] = "name";
    roles[DataRoles::PositionRole] = "position";
    roles[DataRoles::IsEnemyRole] = "is_enemy";
    return roles;
}

int
BulletDataModel::rowCount(const QModelIndex&) const {

    return bullet_data.size();
}

QVariant
BulletDataModel::data(const QModelIndex& index, int role) const {

    if( !index.isValid() ) return QVariant();

    const auto& data = bullet_data[index.row()];
    if( role == NameRole )      return QVariant(data.bulletname);
    if( role == PositionRole )  return QVariant(data.pos);
    if( role == IsEnemyRole )   return QVariant(data.is_enemy);

    return QVariant();
}

void BulletDataModel::moveBullet(const QString& name, const QPoint& dir) {
    b_pos=dir;
    auto itr = findBullet(name);
    if(std::get<0>(itr) == std::end(bullet_data))
        return;

    qDebug() << "   Old position "<<std::get<0>(itr)->pos;
    // std::get<0>(itr)->pos += dir;
    float x=dir.x();
    float y=dir.y();
    qDebug()<<"x="<<x<<","<<"y="<<y;
    std::get<0>(itr)->pos = dir;
    qDebug() << "   New position "<<std::get<0>(itr)->pos;

    emit dataChanged(std::get<1>(itr),std::get<1>(itr));
    //This signal is emitted whenever the data in an existing item changes.
     startTimer(200);
}
void BulletDataModel::shootBullet(const QString& name, const QPoint& dir,gp_game_update old_update) {

    auto itr = findBullet(name);
    if(std::get<0>(itr) == std::end(bullet_data))
        return;

    qDebug() << "   Old bullet position "<<std::get<0>(itr)->pos;
    // std::get<0>(itr)->pos += dir;
    // int x1=dir.x();
    // int y1=dir.y();
    float x2,y2;
    // //////////////
    for(unsigned int i=0;i<old_update.count;i++)

    {
        if(old_update.list[i].id==name.toUInt())
        {
            x2=old_update.list[i].matrix.getM31();

            y2=old_update.list[i].matrix.getM32();

        }
    }
    // qDebug()<<"x="<<x<<","<<"y="<<y;
    // check huong di
    if(dir.x()==0 && dir.y()==-8)  //go up
    {
        std::get<0>(itr)->pos=QPoint(x2,y2-20);
        startTimer(500);
    }
    else if(dir.x()==0 && dir.y()==8)//go down
    {
        std::get<0>(itr)->pos=QPoint(x2,y2+20);
        startTimer(500);
    }
    else if(dir.x()==-8 && dir.y()==0)//turn left
    {
        std::get<0>(itr)->pos=QPoint(x2-20,y2);
        startTimer(500);
    }
    else if(dir.x()==8 && dir.y()==0)//turn right
    {
        std::get<0>(itr)->pos=QPoint(x2+20,y2);
        startTimer(500);
    }


    qDebug() << "   New bullet position "<<std::get<0>(itr)->pos;

    emit dataChanged(std::get<1>(itr),std::get<1>(itr));
    //This signal is emitted whenever the data in an existing item changes.

}
void  BulletDataModel::timerEvent(QTimerEvent *event){
    auto itr = findBullet(b_name);
    std::get<0>(itr)->pos += QPoint(b_pos.x()*1.01,b_pos.y()*1.01)  ;
    emit dataChanged(std::get<1>(itr),std::get<1>(itr));


}
