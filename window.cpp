#include "window.h"
#include <QQmlContext>  //define a context within a QML engine
#include <QQuickItem> //provides basic of visual items in Qt Quick
#include <QDebug>    //output stream for debugging information

Window::Window(QWindow *parent) : QQuickView(parent) {

    client_id=0;
    game_port=0;
    is_enemy=true;
    map_height=0;
    map_width=0;
    rootContext()->setContextProperty("player_model",&_data_model);
    rootContext()->setContextProperty("bullet_model",&_data_bullet);

    setSource (QUrl("qrc:/qml/main-qml"));

    setResizeMode(QQuickView::SizeRootObjectToView);  // dieu chinh kich co cua man hinh xuat hien voi mt
    setMinimumSize(QSize(900,600)); //width,height

    //tao them khi co file qml
    rootObject()->setProperty("client_id",this->get_client_id());
    rootObject()->setProperty("game_port",this->get_game_port());
    rootObject()->setProperty("game_ip",this->get_game_ip());
    rootObject()->setProperty("error",this->get_error());

    //ket noi game server khi qml chuyen signal
    connect(rootObject(), SIGNAL(connectServer(QString,QString)), this, SLOT(connectToServer(QString,QString)));
    connect(rootObject(), SIGNAL(connectGameServer(QString,int,QString)), this, SLOT(connectToGameServer(QString,int,QString)));
    connect(rootObject(), SIGNAL(disconnectGameServer()), this, SIGNAL(signdisconnectGameServer()));
    connect(rootObject(), SIGNAL(disconnectGameServer()), this, SLOT(close()));
    connect(this,SIGNAL(connectToServerStatus(bool)), rootObject(), SIGNAL(connectToServerStatus(bool)));
    connect(rootObject(), SIGNAL(disconnectServer()), this, SIGNAL(signdisconnectServer()));


    connect( rootObject(), SIGNAL(movePlayer(QString,QPointF)),this, SIGNAL(signmovePlayer(QString,QPointF)) );
    connect( rootObject(), SIGNAL(shootPlayer(QString,QPointF)),this, SIGNAL(signshootPlayer(QString,QPointF)) );

}


void Window::connectToServer(QString address,QString port)
{
    qDebug() << "Pressed connect to server button";
    qDebug() <<" ip:" << address;
    qDebug() <<" port:" << port;

    emit signconnectToServer( address, port);
}

void Window::connectToGameServer(QString address,int port,QString name){
    qDebug() << "Pressed connect to server button";

    emit signconnectToGameServer( address, port,name);
}

unsigned int Window::get_client_id()
{

    return client_id;
}
void Window::show_client_id(gp_uint32 id ){

    client_id= id;
    rootObject()->setProperty("client_id", client_id);
}

int Window::get_game_port(){
    return game_port;
}

void Window::show_game_port(int port ){

    game_port= port;
    rootObject()->setProperty("game_port", game_port);
}

void Window::get_map(int height,int width){
    map_height=height;
    map_width=width;
    rootObject()->setProperty("map_height", map_height);
    rootObject()->setProperty("map_width", map_width);
    qDebug() <<"map size: height:"<<map_height<<",width"<<map_width;

}
QString Window::get_game_ip(){
    return game_ip;
}

void Window::show_game_ip(QString ip){

    game_ip=ip;
    rootObject()->setProperty("game_ip",game_ip);

    qDebug() << "game ip sent to window class:"<< ip;
}

QString Window::get_error(){
    return error_connect;
}

void Window::show_error(QString error){

    error_connect=error;
    rootObject()->setProperty("error",error_connect);

    qDebug() << "show error"<< error;
}


void Window::handleUpdateGame(gp_game_update update,QString player_name){
    gp_game_update old_update;
    qDebug() << "my id :" <<this->get_client_id();
    qDebug() << "my name :" <<player_name;


    for(unsigned int i=0;i<update.count;i++)

    {
        qDebug() <<"the information :" <<i;
        qDebug() <<" + id :" << update.list[i].id;
        qDebug() <<" +type :" << update.list[i].type;
        qDebug() <<" +game object type :" << update.list[i].obj_type;// ==player
        qDebug() <<" +position x :"<<update.list[i].matrix.getM31();
        qDebug() <<" +position y :"<<update.list[i].matrix.getM32();
        qDebug() <<" +matrix :";
        qDebug() <<update.list[i].matrix.getM11()<<" "<<update.list[i].matrix.getM21()<<" "<<update.list[i].matrix.getM31();
        qDebug() <<update.list[i].matrix.getM12()<<" "<<update.list[i].matrix.getM22()<<" "<<update.list[i].matrix.getM32();
        qDebug() <<update.list[i].matrix.getM13()<<" "<<update.list[i].matrix.getM23()<<" "<<update.list[i].matrix.getM33();
        if(update.list[i].id==this->get_client_id())
        {
            is_enemy=false;
        }
        else {
            is_enemy=true;


        }


        if( update.list[i].type == QString("move") ) {
            old_update=update;

            _data_model.movePlayer(QString::number(update.list[i].id),QPoint(update.list[i].matrix.getM31(),update.list[i].matrix.getM32()));
            _data_bullet.moveBullet(QString::number(update.list[i].id),QPoint(update.list[i].matrix.getM31(),update.list[i].matrix.getM32()));

        }
        else if( update.list[i].type == QString("spawn") ) {

            _data_model.addPlayer(QString::number(update.list[i].id),QPoint(update.list[i].matrix.getM31(),update.list[i].matrix.getM32()),100, is_enemy);
           _data_bullet.addBullet(QString::number(update.list[i].id),QPoint(update.list[i].matrix.getM31(),update.list[i].matrix.getM32()),is_enemy);

            old_update=update;
        }
        else if( update.list[i].type == QString("die") ) {

            _data_model.removePlayer(QString::number(update.list[i].id));
           _data_bullet.removeBullet(QString::number(update.list[i].id));
        }
        else if( update.list[i].type == QString("shoot")){

            qDebug()<<" you shoot...................";
           _data_bullet.shootBullet(QString::number(update.list[i].id),
                                    QPoint(update.list[i].matrix.getM31(),update.list[i].matrix.getM32()),old_update);

        }
    }

}


