#ifndef CLIENT_H
#define CLIENT_H

#include "gameprotocol.h"
#include "window.h"

#include <QObject>
#include <QTcpSocket>
#include <QHostAddress>



class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(QObject *parent = 0);
    ~Client();


public slots:
    void                  connectServer(QString address, QString port);
    void                  sendConnectRequestToServer( gp_connect_request&  query );
    void                  receiveDataFromServer();
    void                  sendServerQuery(gp_default_server_query& query);
    void                  sendJoinRequest(gp_join_request& query);
    void                  disconnectServer();
    void                  disconnectGame();

    //game
    void                    connectGame(QString game_ip,int game_port);

    void                    JoinRequest(QString address, int port,QString name);
    void                    receiveGameServer();
    void                    disconnectGameServer();

    void                    movePlayer(QString id,QPointF pos);
    void                    sendGameRequest( gp_game_request& request );
    void                    buildGameRequest(QString &cmd,QStringList &params);
    void                    shootPlayer(QString id,QPointF pos);


signals:
    void                signHandleConnectAnswerPkg( gp_header header, gp_connect_answer answer );
    void                signHandleDSQAnswerPkg( gp_header header, gp_default_server_query_answer answer );
    void                signHandleJoinAnswerPkg( gp_header header, gp_join_answer answer );
    void                signGameIp(QString game_ip);
    void                signError(QString error);
    void                signConnectStatus(bool status);

    void                signUpdateGame(gp_game_update update,QString player_name);

private slots:
    void                connectedToServer();
    void                disconnectedToServer();
    void                sentSth() ;
    void                connectedToGameServer();
    void                disconnectedGameServer();

private:

    int                 time;  //de in ra xem moi lan goi gi
    bool                connection;
    QTcpSocket          socket;
    quint32             request_id;
    bool                read_header;
    quint32             size_of_body_pkg;
    gp_header           client_header;
    gp_header_prefix    client_header_prefix;

    quint32             client_id; // can co de khi server tra loi connect requet, client_id co gia tri moi


    //game
    QString            game_ip;
    quint16            game_port;//get in handle server query answer
    quint32            valid_code; //get in join request answer
    QTcpSocket         game_socket;
    quint32            game_request_id;
    bool               game_read_header;
    quint32            game_size_body;
    gp_header          game_client_header;
    gp_header_prefix   game_header_prefix;
    bool               game_connection;
    QString            player_name;
    int                map_height;
    int                map_width;

};

#endif
