#ifndef WINDOW_H
#define WINDOW_H
#include "gameprotocol.h"
#include "playerdatamodel.h"
#include "bulletdatamodel.h"

#include <QQuickView>   //a window displays Qt Quick user interface
#include <QModelIndex> //locate data in a data model
#include <QPointF> //define a floating point


class Window : public QQuickView {
    Q_OBJECT;

public :
    using QQuickView::QQuickView;

    explicit Window(QWindow* parent = 0);
    void     show_client_id(gp_uint32 id );
    void     show_game_port(int port);
    void     get_map(int height,int width);

    unsigned int  get_client_id();
    int      get_game_port();
    void     show_game_ip(QString ip);
    QString  get_game_ip();
    void     show_error(QString error);

    QString  get_error();


 public slots:

    void connectToServer( QString address, QString port);
    void connectToGameServer( QString address, int port,QString name);
    void handleUpdateGame(gp_game_update update,QString player_name);

signals:

    void signconnectToServer(QString address, QString port);
    void signconnectToGameServer(QString address,int port,QString name);
    void signdisconnectGameServer();
    void signdisconnectServer();

    void connectToServerStatus(bool status);
    void signmovePlayer(QString id,QPointF pos);
    void signshootPlayer(QString id,QPointF pos);

private:
   gp_uint32             client_id;
   int                   game_port;
   QString               game_ip;
   QString               error_connect;

   bool                  is_enemy;

   PlayerDataModel       _data_model;
   BulletDataModel       _data_bullet;
   int                   map_height; // get the map size from server
   int                   map_width;


};
#endif // WINDOW_H
