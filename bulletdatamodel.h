#ifndef BULLETDATAMODEL_H
#define BULLETDATAMODEL_H


// qt
#include <QPoint>
#include <QAbstractListModel>
#include <QDebug>
#include <QTimer>
#include "gameprotocol.h"
//stl
#include<algorithm>


namespace Private {
struct BulletData {
    explicit BulletData() : bulletname{}, pos{},is_enemy {}{}

    explicit BulletData(const QString& bulletname_in,const QPoint& pos_in,bool is_enemy_in)

        : bulletname{bulletname_in},pos{pos_in},is_enemy{is_enemy_in} {}

    QString       bulletname;
    QPoint        pos;
    bool          is_enemy;
};
}

class BulletDataModel : public QAbstractListModel {
    Q_OBJECT
public:
    enum DataRoles : int {
        NameRole = Qt::UserRole + 1,
        PositionRole,
        IsEnemyRole
    };

    using QAbstractListModel::QAbstractListModel;

    void                            addBullet( const QString& name, const QPoint& pos,bool is_enemy = true);
    void                            removeBullet( const QString& name);
    QHash<int, QByteArray>          roleNames() const override;
    int                             rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant                        data(const QModelIndex &index, int role) const override;
    void                            timerEvent(QTimerEvent *event);


public slots:
    void                            moveBullet( const QString& name, const QPoint& dir);
    void                            shootBullet( const QString& name, const QPoint& dir,gp_game_update old_update);

private:
    using PlayerDataTuple = std::tuple<QVector<Private::BulletData>::Iterator,QModelIndex>;

    PlayerDataTuple                 findBullet( const QString& name );

    QVector<Private::BulletData>    bullet_data;
    QString                         b_name;
    QPoint                          b_pos;


};

#endif // BULLETDATAMODEL_H
