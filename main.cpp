
#include "guiapplication.h"
#include "client.h"


int main( int argc, char *argv[] ) {

    GuiApplication a(argc,argv );
    a.initialize();

    Client client;
    QObject::connect(&a,SIGNAL(signconnect(QString,QString)),&client,SLOT(connectServer(QString,QString)));

    QObject::connect( &client, SIGNAL(signHandleConnectAnswerPkg(gp_header,gp_connect_answer)), &a, SLOT(handleConnectAnswerPkg(gp_header,gp_connect_answer)) );
    QObject::connect( &client, SIGNAL(signHandleDSQAnswerPkg(gp_header,gp_default_server_query_answer)), &a, SLOT(handleDSQAnswerPkg(gp_header,gp_default_server_query_answer)) );
    QObject::connect( &client, SIGNAL(signGameIp(QString)), &a, SLOT(handleGameIp(QString)) );
    QObject::connect( &client, SIGNAL(signError(QString)), &a, SLOT(handleError(QString)) );
    QObject::connect( &client, SIGNAL(signConnectStatus(bool)), &a, SIGNAL(signConnectStatus(bool)) );
    QObject::connect( &client, SIGNAL(signUpdateGame(gp_game_update,QString)), &a, SIGNAL(signUpdateGame(gp_game_update,QString)) );

    QObject::connect(&a,SIGNAL(signconnectGame(QString,int,QString)),&client,SLOT(JoinRequest(QString,int,QString)));
    QObject::connect(&a,SIGNAL(signdisconnectGame()),&client,SLOT(disconnectGame()));
    QObject::connect(&a,SIGNAL(signdisconnect()),&client,SLOT(disconnectServer()));
    QObject::connect(&a,SIGNAL(signmovePlayer(QString,QPointF)),&client,SLOT(movePlayer(QString,QPointF)));
    QObject::connect(&a,SIGNAL(signshootPlayer(QString,QPointF)),&client,SLOT(shootPlayer(QString,QPointF)));
    return a.exec();


    return 0;
}
