#include"guiapplication.h"

GuiApplication::GuiApplication(int& argc, char** argv): QGuiApplication(argc, argv),_window() {


    connect(&_window,SIGNAL(signconnectToServer(QString , QString)),this,SIGNAL(signconnect(QString ,QString)));
    connect(&_window,SIGNAL(signconnectToGameServer(QString ,int,QString)),this,SIGNAL(signconnectGame(QString ,int,QString)));
    connect(&_window,SIGNAL(signdisconnectGameServer()),this,SIGNAL(signdisconnectGame()));
    connect(&_window,SIGNAL(signdisconnectServer()),this,SIGNAL(signdisconnect()));
    connect(&_window,SIGNAL(signmovePlayer(QString,QPointF)),this,SIGNAL(signmovePlayer(QString,QPointF)));
    connect(&_window,SIGNAL(signshootPlayer(QString,QPointF)),this,SIGNAL(signshootPlayer(QString,QPointF)));

    connect(this, SIGNAL(signConnectStatus(bool)),&_window,SIGNAL(connectToServerStatus(bool)));
    connect(this, SIGNAL(signUpdateGame(gp_game_update,QString)),&_window,SLOT(handleUpdateGame(gp_game_update,QString)));
}


void GuiApplication::initialize(){

    _window.show();
}

void GuiApplication::handleConnectAnswerPkg( gp_header header, gp_connect_answer answer ) {

   _window.show_client_id( answer.client_id );

}

void GuiApplication::handleDSQAnswerPkg(gp_header header,gp_default_server_query_answer answer){
   _window.show_game_port(answer.server_info.connect_port);
   _window.get_map(answer.map_info.height,answer.map_info.width);

}
 void GuiApplication::handleGameIp(QString game_ip){

    _window.show_game_ip(game_ip);
}
 void GuiApplication::handleError(QString error){

     _window.show_error(error);
 }
