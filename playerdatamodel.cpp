#include "playerdatamodel.h"

void
PlayerDataModel::addPlayer(const QString& name, const QPoint& pos,int score, bool is_enemy) {
    qDebug() << "rowcount before insert :" <<rowCount();
    int a;
    if(_data.size()>0)
    {
        for(int i=0;i<_data.size();i++)
        {
            if(_data[i].name==name)
            { a=1;  }
            else { a=0;}
        }


    }
    else if(_data.size()==0)

    {
        beginInsertRows(QModelIndex(),rowCount(),rowCount());
        _data.append(Private::PlayerData(name,pos,score,is_enemy));
        endInsertRows();
    }
    if(a==0){
        beginInsertRows(QModelIndex(),rowCount(),rowCount());
        _data.append(Private::PlayerData(name,pos,score,is_enemy));
        endInsertRows();
    }
    qDebug() << "rowcount after insert :" <<rowCount();
    qDebug() << "new pos in client's view :" <<pos.x()<<" "<<pos.y();

}
void PlayerDataModel::removePlayer(const QString& name) {

    qDebug() << "rowcount before remove :" <<rowCount();
    for(int i=0;i<_data.size();i++){
        if(_data[i].name==name){
            beginRemoveRows(QModelIndex(),i,i);
            _data.remove(i);
            emit(dataChanged(index(i), index(i))); //update the view
            endRemoveRows();
        }


    }
    qDebug() << "rowcount after remove :" <<rowCount();
}
PlayerDataModel::PlayerDataTuple
PlayerDataModel::findPlayer(const QString& name) {

    auto item = std::find_if( std::begin(_data), std::end(_data),
                              [name](const Private::PlayerData& data) { return data.name == name; }
    );

    return PlayerDataTuple(item,index(item-std::begin(_data)));
}


QHash<int,QByteArray>
PlayerDataModel::roleNames() const {

    QHash<int,QByteArray> roles;
    roles[DataRoles::NameRole] = "name";
    roles[DataRoles::PositionRole] = "position";
    roles[DataRoles::IsEnemyRole] = "is_enemy";
    roles[DataRoles::ScoreRole] = "score";
    return roles;
}

int
PlayerDataModel::rowCount(const QModelIndex&) const {

    return _data.size();
}

QVariant
PlayerDataModel::data(const QModelIndex& index, int role) const {

    if( !index.isValid() ) return QVariant();

    const auto& data = _data[index.row()];
    if( role == NameRole )      return QVariant(data.name);
    if( role == PositionRole )  return QVariant(data.pos);
    if( role == ScoreRole )     return QVariant(data.score);
    if( role == IsEnemyRole )   return QVariant(data.is_enemy);

    return QVariant();
}

void PlayerDataModel::movePlayer(const QString& name, const QPoint& dir) {

    auto itr = findPlayer(name);
    if(std::get<0>(itr) == std::end(_data))
        return;

    qDebug() << "   Old pos "<<std::get<0>(itr)->pos;
    std::get<0>(itr)->pos = dir;
    qDebug() << "   New pos "<<std::get<0>(itr)->pos;

    emit dataChanged(std::get<1>(itr),std::get<1>(itr));
    //This signal is emitted whenever the data in an existing item changes.
}

